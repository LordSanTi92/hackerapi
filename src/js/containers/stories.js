import React from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import {
    fetchStoriesId,
    fetchStory,
    fetchKarma,
    sortStories,
    makeChartData,
    clearAll,
    loadedAllData,
    clearCommentsArray
} from "../actions/actions";
import {Button, CardColumns} from 'reactstrap';
import PropTypes from "prop-types";
import StoryListElement from '../components/story_list_element';

class Stories extends React.Component {
    static propTypes = {
        idsOfStories: PropTypes.array,
        arrayOfStories: PropTypes.array,
        arrayOfUsersKarma: PropTypes.array,
        sortedStories: PropTypes.array,
        fetchStoriesId: PropTypes.func,
        fetchStory: PropTypes.func,
        fetchKarma: PropTypes.func,
        sortStories: PropTypes.func,
        makeChartData: PropTypes.func,
        loadedAllData: PropTypes.func,
        clearAllData: PropTypes.func,
        clearCommentsArray: PropTypes.func,
        loadNewData: PropTypes.func,
    };

    handleClickReload = () => {
        new Promise(resolve => {
            resolve(this.props.clearAll());
        }).then(() => this.props.loadNewData());
    };

    componentDidMount() {
        this.props.clearCommentsArray();
    };

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1>Top Stories of HackerNews API</h1>
                        <nav>
                            <Link to="/chart">
                                <Button id="chart">Chart view</Button>
                            </Link>
                            <Button id="reload" onClick={this.handleClickReload}>Load another top stories</Button>
                        </nav>
                        <CardColumns>
                            {this.props.sortedStories.map((storyObj, index) => {
                                let userKarma;
                                this.props.arrayOfUsersKarma.map(elem => Object.values(elem).includes(storyObj.by) ?
                                    userKarma = elem.karma
                                    : null);
                                return <StoryListElement storyObj={storyObj} key={index} index={index + 1}
                                                         karma={userKarma}/>
                            })}
                        </CardColumns>
                    </div>
                </div>
            </div>
        );
    };
}

const mapStateToProps = state => {
    return {
        idsOfStories: state.idsOfStories,
        arrayOfStories: state.arrayOfStories,
        arrayOfUsersKarma: state.arrayOfUsersKarma,
        sortedStories: state.sortedStories,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchStoriesId,
            fetchStory,
            fetchKarma,
            sortStories,
            makeChartData,
            loadedAllData,
            clearAll,
            clearCommentsArray
        }, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Stories);