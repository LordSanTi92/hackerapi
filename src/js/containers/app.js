import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {HashRouter, Route, Switch} from "react-router-dom";
import {fetchStoriesId, fetchStory, fetchKarma, sortStories, makeChartData, loadedAllData} from "../actions/actions";
import Stories from './stories.js';
import StoryDetails from './story_details';
import LoadingCircle from '../components/loading_circle';
import Chart from '../components/chart';
import NoMatch from '../components/no_match';

class App extends React.Component {

    static propTypes = {
        idsOfStories: PropTypes.array,
        arrayOfStories: PropTypes.array,
        arrayOfUsersKarma: PropTypes.array,
        sortedStories: PropTypes.array,
        progress: PropTypes.string,
        fetchStoriesId: PropTypes.func,
        fetchStory: PropTypes.func,
        fetchKarma: PropTypes.func,
        makeChartData: PropTypes.func,
        loadedAllData: PropTypes.func,
    };

    componentDidMount() {
        this.fillReduxStore();
    }

    fillReduxStore = () => {
        new Promise((resolve) => {
            resolve(this.props.fetchStoriesId());
        }).then(response => {
            for (let i = 0; i < response.value.length; i++) {
                new Promise((resolve) => {
                    resolve(this.props.fetchStory(response.value[i]));
                }).then(response => {
                    return this.props.fetchKarma(response.value.by);
                }).then(() => {
                    if (this.props.arrayOfUsersKarma.length === 10) {
                        this.props.sortStories(this.props.arrayOfStories);
                        this.props.makeChartData(this.props.arrayOfStories);
                        this.props.loadedAllData();
                    }
                });
            }
        });
    };

    render() {
        return (
            <HashRouter>
                <Switch>
                    <Route exact path="/" render={() => this.props.progress === "Loaded" ?
                        <Stories loadNewData={this.fillReduxStore}/> : <LoadingCircle/>}/>
                    <Route path="/story/:id"
                           render={match => this.props.progress === "Loaded" ? <StoryDetails match={match}/> :
                               <LoadingCircle/>}/>
                    <Route path="/chart" render={() => this.props.progress === "Loaded" ? <Chart/> : <LoadingCircle/>}/>
                    <Route path="*" component={NoMatch}/>
                </Switch>
            </HashRouter>
        );
    };
}

const mapStateToProps = state => {
    return {
        idsOfStories: state.idsOfStories,
        arrayOfStories: state.arrayOfStories,
        arrayOfUsersKarma: state.arrayOfUsersKarma,
        sortedStories: state.sortedStories,
        progress: state.progress
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {fetchStoriesId, fetchStory, fetchKarma, sortStories, makeChartData, loadedAllData}, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);