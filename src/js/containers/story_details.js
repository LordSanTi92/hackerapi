import React, {Fragment} from 'react';
import {connect} from "react-redux";
import {
    Card, Button, Badge, CardFooter, CardBody,
    CardTitle, CardText
} from 'reactstrap';
import {Link, Redirect} from "react-router-dom";
import {fetchComments} from "../actions/actions";
import {bindActionCreators} from "redux";
import LoadingCircle from '../components/loading_circle';
import CommentSection from "../components/comment_section";
import PropTypes from "prop-types";


class StoryDetails extends React.Component {

    static propTypes = {
        arrayOfUsersKarma: PropTypes.array,
        sortedStories: PropTypes.array,
        arrayOfComments: PropTypes.array,
        match: PropTypes.object,
        data: PropTypes.object,
        fetchStoriesId: PropTypes.func,
        fetchComments: PropTypes.func,
    };

    componentDidMount() {
        if(this.props.match.match.params.id > 10 || this.props.match.match.params.id < 1) {
            return <Redirect to='/error'/>;
        }
        const id = this.props.match.match.params.id - 1;
        const data = this.props.sortedStories[id];
        data.kids ? this.props.fetchComments(data.kids) : null;
    };

    render() {
        if(this.props.match.match.params.id > 10 || this.props.match.match.params.id < 1) {
            return <Redirect to='/error'/>;
        }
        const id = this.props.match.match.params.id - 1;
        const data = this.props.sortedStories[id];
        const date = new Date(data.time * 1000).toLocaleDateString();
        let karma;
        this.props.arrayOfUsersKarma.map(elem => Object.values(elem).includes(data.by) ? karma = elem.karma : null);
        let showComments = (<div className="inside-loader">
            <LoadingCircle/>
        </div>);
        switch (true) {
            case (this.props.arrayOfComments.length === 0 && !data.kids):
                showComments = <h3>Comment list is empty!</h3>;
                break;
            case (this.props.arrayOfComments.length > 0):
                showComments = (<Fragment>
                    <h3>Comments:</h3>
                    {this.props.arrayOfComments.map((elem, index) => <CommentSection key={index} index={index + 1}
                                                                                     commentObj={elem}/>)}
                </Fragment>);
                break;
        }
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1>{`Story #${id + 1}`}</h1>
                        <nav>
                            <Link to="/">
                                <Button id="reload">Go back to card view</Button>
                            </Link>
                        </nav>
                        <Card>
                            <CardBody>
                                <CardTitle>{data.title}</CardTitle>
                                <CardText>By: {data.by}</CardText>
                                <div className="score-and-karma">
                                    <Button color="info" outline>
                                        Score <Badge color="info">{data.score}</Badge>
                                    </Button>
                                    <Button color="danger" outline>
                                        Karma <Badge color="danger">{karma}</Badge>
                                    </Button>
                                    <Button color="light">
                                        Created <Badge color="light">{date}</Badge>
                                    </Button>
                                </div>
                            </CardBody>
                            <CardFooter>
                                <a href={data.url} target="_blank">
                                    <Button color="dark">Open story in new tab</Button>
                                </a>
                            </CardFooter>
                        </Card>
                        <div className="comments-section">
                            {showComments}
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

const mapStateToProps = state => {
    return {
        sortedStories: state.sortedStories,
        arrayOfUsersKarma: state.arrayOfUsersKarma,
        arrayOfComments: state.arrayOfComments,
    };
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {fetchComments}, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StoryDetails);