import { combineReducers } from 'redux';
import  FetchStoriesId  from "./reducer_fetch_stories_id";
import  FetchStories  from "./reducer_fetch_story";
import  FetchUsersKarma  from "./reducer_fetch_user_karma";
import  FetchComments  from "./reducer_fetch_comments";
import  SortedStories  from "./reducer_sorted_stories";
import  ChartData  from "./reducer_chart_data";
import  ChangeView  from "./reducer_view";

const rootReducer = combineReducers({
    idsOfStories: FetchStoriesId,
    arrayOfStories: FetchStories,
    arrayOfUsersKarma: FetchUsersKarma,
    arrayOfComments: FetchComments,
    sortedStories: SortedStories,
    chartData: ChartData,
    progress: ChangeView,
});

export default rootReducer;