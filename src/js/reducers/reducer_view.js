import {CLEAR, DONE} from "../actions/actions";

export default function (state = "Loading", action) {
    switch (action.type) {
        case `${CLEAR}_FULFILLED`:
            return action.payload;
        case DONE:
            return action.payload;
        default:
            return state;
    }
};