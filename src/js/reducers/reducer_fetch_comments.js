import {CLEAR, CLEAR_COMMENTS_ARRAY, FETCH_COMMENTS} from "../actions/actions";


export default function (state = [], action) {
    switch (action.type) {
        case `${CLEAR}_FULFILLED`:
            return [];
        case `${FETCH_COMMENTS}_PENDING`:
            return [];
        case `${FETCH_COMMENTS}_FULFILLED`:
            return action.payload;
        case `${FETCH_COMMENTS}_REJECTED`:
            return [];
        case CLEAR_COMMENTS_ARRAY:
            return [];
        default:
            return state;
    }
};