import {CLEAR, FETCH_STORY} from "../actions/actions";

export default function (state = [], action) {
    switch (action.type) {
        case `${CLEAR}_FULFILLED`:
            return [];
        case `${FETCH_STORY}_PENDING`:
            return state;
        case `${FETCH_STORY}_FULFILLED`:
            return [...state, action.payload];
        default:
            return state;
    }
};