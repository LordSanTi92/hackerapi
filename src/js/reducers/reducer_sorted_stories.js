import {CLEAR, SORT_STORIES} from "../actions/actions";

export default function (state = null, action) {
    switch (action.type) {
        case `${CLEAR}_FULFILLED`:
            return [];
        case SORT_STORIES:
            return action.payload;
        default:
            return state;
    }
};