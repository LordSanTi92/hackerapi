import {CHART_DATA, CLEAR} from "../actions/actions";

export default function (state = null, action) {
    switch (action.type) {
        case `${CLEAR}_FULFILLED`:
            return null;
        case CHART_DATA:
            return action.payload;
        default:
            return state;
    }
};