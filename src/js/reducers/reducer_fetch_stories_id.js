import {CLEAR, FETCH_STORIES_ID} from "../actions/actions";

export default function (state = [], action) {
    switch (action.type) {
        case `${CLEAR}_FULFILLED`:
            return null;
        case `${FETCH_STORIES_ID}_PENDING`:
            return state;
        case `${FETCH_STORIES_ID}_REJECTED`:
            return [];
        case `${FETCH_STORIES_ID}_FULFILLED`:
            return action.payload;
        default:
            return state;
    }
};