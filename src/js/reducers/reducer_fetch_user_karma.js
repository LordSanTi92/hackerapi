import {CLEAR, FETCH_KARMA} from "../actions/actions";


export default function (state = [], action) {
    switch (action.type) {
        case `${CLEAR}_FULFILLED`:
            return [];
        case `${FETCH_KARMA}_PENDING`:
            return state;
        case `${FETCH_KARMA}_FULFILLED`:
            return [...state, action.payload];
        case `${FETCH_KARMA}_REJECTED`:
            return [];
        default:
            return state;
    }
};