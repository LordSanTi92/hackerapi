import axios from 'axios';

export const FETCH_STORIES_ID = 'FETCH_STORIES_ID';
export const FETCH_STORY = 'FETCH_STORY';
export const FETCH_KARMA = 'FETCH_KARMA';
export const FETCH_COMMENTS = 'FETCH_COMMENTS';
export const SORT_STORIES = 'SORT_STORIES';
export const CHART_DATA = 'CHART_DATA';
export const DONE = 'DONE';
export const CLEAR = 'CLEAR';
export const CLEAR_COMMENTS_ARRAY = 'CLEAR_COMMENTS_ARRAY';


export function fetchStoriesId() {
    const requestTopStoriesId = axios({
        method: 'get',
        url: 'https://hacker-news.firebaseio.com/v0/topstories.json'
    }).then(response => {
        const arrLength = response.data.length;
        const randNumber = Math.floor(Math.random() * arrLength - 10);
        return response.data.slice(randNumber, randNumber + 10);
    }).catch(error => {
        window.location.hash = "#error";
    });
    return {type: FETCH_STORIES_ID, payload: requestTopStoriesId};
}

export function fetchStory(item) {
    const stories = axios({
        method: 'get',
        url: `https://hacker-news.firebaseio.com/v0/item/${item}.json`
    }).then(response => {
        return response.data;
    }).catch(error => {
        window.location.hash = "#error";
    });
    return {type: FETCH_STORY, payload: stories};
}

export function fetchKarma(userId) {
    const userKarma = axios({
        method: 'get',
        url: `https://hacker-news.firebaseio.com/v0/user/${userId}.json`
    }).then(response => {
        return {user: response.data.id, karma: response.data.karma};
    }).catch(error => {
        window.location.hash = "#error";
    });
    return {type: FETCH_KARMA, payload: userKarma};
}

export function fetchComments(story) {
    const arrayOfCommentsIds = story.slice(0, 10);
    const arrayOfComments = [];
    const arrayOfUsers = [];
    const comments = new Promise(resolve => {
        for (let i = 0; i < arrayOfCommentsIds.length; i++) {
            new Promise(resolve => {
                resolve(axios({
                    method: 'get',
                    url: `https://hacker-news.firebaseio.com/v0/item/${arrayOfCommentsIds[i]}.json`
                }));
            }).then(response => {
                arrayOfComments.push(response.data);
                return axios({
                    method: 'get',
                    url: `https://hacker-news.firebaseio.com/v0/user/${response.data.by}.json`
                });
            }).then(response => {
                arrayOfUsers.push(response.data);
            }).then(response => {
                if (arrayOfUsers.length === arrayOfComments.length) {
                    arrayOfComments.map(commentObj => {
                        Object.values(commentObj).includes(arrayOfUsers.map(userObj => {
                            if (commentObj.by === userObj.id) {
                                commentObj.submitted = userObj.submitted;
                            }
                        }));
                    });
                    arrayOfComments.map((commentObj, index) => {
                        commentObj.deleted === true ? arrayOfComments.splice(index, 1) : null;
                    });
                    resolve(arrayOfComments);
                }
            }).catch(error => {
                window.location.hash = "#error";
            });
        }
    });
    return {type: FETCH_COMMENTS, payload: comments};
}


export function sortStories(stories) {
    const copy = [...stories];
    const sortedStories = copy.sort((a, b) => {
        if (a.score > b.score)
            return -1;
        if (a.score < b.score)
            return 1;
        return 0;
    });
    return {type: SORT_STORIES, payload: sortedStories};
}

export function makeChartData(data) {
    const labels = [];
    const datasets = [
        {
            label: "Score",
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: []
        }
    ];
    const chartObj = {};
    const chartData = () => {
        data.map(elem => {
            labels.push(elem.by);
            datasets[0].data.push(elem.score);
        });
        chartObj.labels = labels;
        chartObj.datasets = datasets;
        return chartObj;
    };

    return {type: CHART_DATA, payload: chartData};
}

export function loadedAllData() {
    return {type: DONE, payload: "Loaded"};
}

export function clearAll() {
    return {type: CLEAR, payload: Promise.resolve("Loading")};
}

export function clearCommentsArray() {
    return {type: CLEAR_COMMENTS_ARRAY, payload: []};
}