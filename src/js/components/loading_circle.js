import React from 'react';
const LoadingCircle = () =>{
    return (
        <div className="loader-outside">
            <div className="loader-line"></div>
        </div>
    );
};

export default LoadingCircle;