import React from 'react';
import {
    Card, Button, CardHeader, CardFooter, CardBody,
    CardTitle, CardText, Badge
} from 'reactstrap';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


const StoryListElement = props => {
    const date = new Date(props.storyObj.time * 1000).toLocaleDateString();
    return (
        <Card>
            <CardHeader>{`Story #${props.index} created ${date}`}</CardHeader>
            <CardBody>
                <CardTitle>{props.storyObj.title}</CardTitle>
                <CardText>By: {props.storyObj.by}</CardText>
                <div className="score-and-karma">
                    <Button color="info" outline>
                        Score <Badge color="info">{props.storyObj.score}</Badge>
                    </Button>
                    <Button color="danger" outline>
                        Karma <Badge color="danger">{props.karma}</Badge>
                    </Button>
                </div>
            </CardBody>
            <CardFooter>
                <Link to={`/story/${props.index}`}>
                    <Button color="dark">Go to story</Button>
                </Link>
            </CardFooter>
        </Card>
    );
};

StoryListElement.propTypes = {
    storyObj: PropTypes.object,
    index: PropTypes.number,
    karma: PropTypes.number
};

export default StoryListElement;