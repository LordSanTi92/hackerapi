import React, {Fragment} from 'react';
import {
    Card, Button, CardHeader, CardBody, CardText, Badge
} from 'reactstrap';
import ReactHtmlParser from 'react-html-parser';
import PropTypes from 'prop-types';


const CommentSection = props => {
    const date = new Date(props.commentObj.time * 1000).toLocaleDateString();
    return (
        <Fragment>
            <Card>
                <CardHeader>{`Comment #${props.index} created ${date} by ${props.commentObj.by}`}</CardHeader>
                <CardBody>
                    <CardText>{ReactHtmlParser(props.commentObj.text)}</CardText>
                    <div className="score-and-karma">
                        <Button color="info" outline>
                            User's activity <Badge color="info">{props.commentObj.submitted.length}</Badge>
                        </Button>
                    </div>
                </CardBody>
            </Card>
        </Fragment>
    );
};

CommentSection.propTypes = {
    commentObj: PropTypes.object,
    index: PropTypes.number
};

export default CommentSection;