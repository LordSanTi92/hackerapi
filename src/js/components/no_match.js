import React from 'react';
import {Alert, Button} from 'reactstrap';
import {Link} from "react-router-dom";

const NoMatch = props => {
    return (
        <Alert color="danger">
            <h4 className="alert-heading">Oooops!</h4>
            <p>
                Something went wrong and we are really sorry! :(
            </p>
            <p>
                Please check if your URL is correct. If you are trying to get specific story please remember that this is possible only with numbers between 1 to 10.
            </p>
            <p>
                Please click the button below this warning.
            </p>
            <hr/>
            <p style={{"text-align":"center"}} className="mb-0">
                <Link to="/">
                    <Button color="dark">Click Me! :)</Button>
                </Link>
            </p>
        </Alert>
    );
};

export default NoMatch;