import React from 'react';
import {connect} from 'react-redux';
import {Line} from 'react-chartjs-2';
import {Link} from "react-router-dom";
import {Button} from 'reactstrap';
import PropTypes from 'prop-types';

const Chart = props => {
    return (
        <div className="container">
            <div className="row">
                <div className="col">
                    <h1>Chart of Top Stories</h1>
                    <nav>
                        <Link to="/">
                            <Button id="reload">Back to card view</Button>
                        </Link>
                    </nav>
                    <Line data={props.chartData}/>
                </div>
            </div>
        </div>
    );
};

Chart.propTypes = {
    chartData: PropTypes.object
};

const mapStateToProps = state => {
    return {
        chartData: state.chartData
    };
};

export default connect(
    mapStateToProps,
    null
)(Chart);