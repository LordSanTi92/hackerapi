# HackerApi

SPA with React + React Router + Redux from HackerNews API


Download or clone this repository, then type inside project folder inside the terminal

```
npm install
npm start
```

Open a browser and type [http://localhost:3001](http://localhost:3001)



Inside this SPA you can find top 10 randomly picked stories from HackerNews API. There is also the possibility to check
every story details and check the chart of picked stories. 

You can also visit [http://flamboyant-bohr-d09f6f.netlify.com](http://flamboyant-bohr-d09f6f.netlify.com/#/) to check
online version of my app.